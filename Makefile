CC = g++
all: static shared

shared: main.cpp libGeometry.so
	$(CC) $^  -o $@ -pthread -L ./ -l Geometry

static: main.cpp libGeometry.a
	$(CC) $^ -o $@ -pthread  -L ./ -l Geometry

libGeometry.so: Figure.o Rectangle.o 
	$(CC) $^ -o $@ -shared -I ./

libGeometry.a: Figure.o Rectangle.o 
	ar -rcs $@ $^

Rectangle.o: Rectangle.cpp Rectangle.h Figure.h
	$(CC) $< -c -o $@ -fPIC

Figure.o: Figure.cpp Figure.h
	$(CC) $< -c -o $@ -fPIC

clean:
	rm static libGeometry.a Rectangle.o Figure.o libGeometry.so shared
#ifndef FIGURE_H
#define FIGURE_H 1

#include <iostream>

class Figure {
 public:
  Figure();
  virtual void surfaceArea() = 0;
  virtual void Perimeter() = 0;
  int getsurfaceArea();
  int getPerimeter();
  virtual void Method();

 public:
  int m_surfaceArea;
  int m_Perimeter;
};

#endif  // FIGURE_H
